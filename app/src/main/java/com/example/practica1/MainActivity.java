package com.example.practica1;

import android.content.pm.ActivityInfo;
import android.widget.TextView;
import android.view.View;
import android.os.Bundle;
import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    TextView tvButtonRegla3;
    TextView tvButtonTriangle;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        tvButtonRegla3 = (TextView) findViewById(R.id.textView3);
        tvButtonRegla3.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                //finish();
                Intent i = new Intent(MainActivity.this, MainRegla.class);
                startActivity(i);

            }
        });
        tvButtonTriangle = (TextView) findViewById(R.id.textView2);
        tvButtonTriangle.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                //finish();
                Intent i = new Intent(MainActivity.this, MainTriangle.class);
                startActivity(i);

            }
        });
    }
}
